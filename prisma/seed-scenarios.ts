import type { PrismaClient } from "@prisma/client";
import { MessageCategory } from "~/constants";

const SPACE_TRAVEL = `This game will be set in outer space in the distant future.  Players assume the role of a crew of a spaceship.  The crew is on a mission to explore a new and possibly dangerous planet.  In addition to acting at the game master, you will also act as the spaceship's AI system.  When the players wish to talk to the ship's AI character, they will begin their messages with "computer".`;

const CLASSIC_FANTASY = `This game will be set in a high-fantasy setting.  The players assume the role of a traveling band of adventures.`;

async function createScenario({
  prisma,
  content,
  title,
}: {
  content: string;
  title: string;
  prisma: PrismaClient;
}) {
  return prisma.message.create({
    data: {
      title,
      content,
      isTemplate: true,
      category: MessageCategory.Summary.World,
    },
  });
}

export default async function seedScenarios(prisma: PrismaClient) {
  await createScenario({
    prisma,
    title: "Space Travel",
    content: SPACE_TRAVEL,
  });
  await createScenario({
    prisma,
    title: "Classic Fantasy",
    content: CLASSIC_FANTASY,
  });
}
