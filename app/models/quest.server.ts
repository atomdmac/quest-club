import type { Message } from "@prisma/client";
import { MessageCategory } from "~/constants";
import { prisma } from "~/db.server";
import LLM from "~/assistants";

export async function createQuest(
  userId: string,
  title: string,
  worldSummary: string
) {
  // Seed conversations with scenario prompts.
  const quest = await prisma.quest.create({
    data: {
      title,
      owner: {
        connect: {
          id: userId,
        },
      },
      members: {
        connect: {
          id: userId,
        },
      },
      messages: {
        create: [
          {
            content: worldSummary,
            category: MessageCategory.Summary.World,
            user: {
              connect: {
                id: userId,
              },
            },
          },
        ],
      },
    },
  });

  return quest;
}

export async function getAllUserQuests(userId: string) {
  const quests = await prisma.quest.findMany({
    where: {
      members: {
        some: {
          id: userId,
        },
      },
    },
    include: {
      messages: {
        take: 1,
        where: {
          category: MessageCategory.Summary.Story,
        },
        orderBy: {
          createdAt: "desc",
        },
      },
    },
  });

  return quests;
}

export async function assertUserIsInQuest(userId: string, questId: string) {
  const quest = await prisma.quest.findFirst({
    where: {
      id: questId,
      members: {
        some: {
          id: userId,
        },
      },
    },
  });
  if (!quest) {
    throw new Error("User is not a member of this quest");
  }
  return quest;
}

export async function getUserQuest(userId: string, questId: string) {
  const quest = await prisma.quest.findFirst({
    where: {
      id: questId,
      members: {
        some: {
          id: userId,
        },
      },
    },
    include: {
      members: true,
      owner: true,
      messages: {
        where: {
          category: {
            notIn: [
              MessageCategory.Summary.World,
              MessageCategory.Summary.Story,
              MessageCategory.Summary.Character,
              MessageCategory.System.Instructions,
            ],
          },
        },
        orderBy: {
          createdAt: "asc",
        },
        include: {
          user: true,
        },
      },
    },
  });

  if (!quest) {
    throw new Error("Quest not found");
  }
  return quest;
}

export async function createUserMessage(
  userId: string,
  questId: string,
  content: string,
  isMeta: boolean = false
) {
  return await prisma.message.create({
    data: {
      content,
      category: isMeta
        ? MessageCategory.Meta.Player
        : MessageCategory.InGame.Player,
      user: {
        connect: {
          id: userId,
        },
      },
      quest: {
        connect: {
          id: questId,
        },
      },
    },
    include: {
      user: true,
    },
  });
}

export async function getUserOwnsQuest(userId: string, questId: string) {
  const quest = await prisma.quest.findFirst({
    where: {
      id: questId,
      ownerId: userId,
    },
  });
  return !!quest;
}

export async function editQuest(
  userId: string,
  questId: string,
  title: string,
  scenario: string,
  summary: string
) {
  const quest = await getEditableQuest(userId, questId);

  // Update quest fields
  const updatedQuest = await prisma.quest.update({
    where: {
      id: questId,
    },
    data: {
      title,
    },
  });

  // Update scenario message
  if (quest.worldSummary) {
    await prisma.message.update({
      where: {
        id: quest.worldSummary.id,
      },
      data: {
        content: scenario,
      },
    });
  }

  // Update summary message
  if (quest.storySummary) {
    await prisma.message.update({
      where: {
        id: quest.storySummary.id,
      },
      data: {
        content: summary,
      },
    });
  }

  if (!updatedQuest) {
    throw new Error("Unable to update quest!");
  }

  return updatedQuest;
}

export async function getEditableQuest(userId: string, questId: string) {
  const quest = await prisma.quest.findFirst({
    where: {
      id: questId,
      ownerId: userId,
    },
  });

  const worldSummary = await prisma.message.findFirst({
    where: {
      questId,
      category: MessageCategory.Summary.World,
      isTemplate: false,
    },
  });

  const storySummary = await prisma.message.findFirst({
    where: {
      questId,
      category: MessageCategory.Summary.Story,
      isTemplate: false,
    },
  });

  if (!quest) {
    throw new Error("Quest not found");
  }

  return {
    quest,
    worldSummary,
    storySummary,
  };
}

export async function getLatestQuestSummary(questId: string) {
  const storySummary = await prisma.message.findFirst({
    where: {
      questId,
      category: MessageCategory.Summary.Story,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  // If there is no story summary, return the world summary.
  if (!storySummary) {
    return await prisma.message.findFirst({
      where: {
        questId,
        category: MessageCategory.Summary.World,
      },
      orderBy: {
        createdAt: "desc",
      },
    });
  }

  return storySummary;
}

/**
 * Return the data needed to summarize a quest using the LLM.
 */
export async function getQuestSummaryForLLM(questId: string, userId: string) {
  const worldSummary = await prisma.message.findMany({
    where: {
      questId,
      category: MessageCategory.Summary.World,
    },
    orderBy: {
      createdAt: "asc",
    },
    include: {
      user: true,
    },
  });

  if (!worldSummary) {
    throw new Error("Quest introduction not found");
  }

  const mostRecentSummary = await prisma.message.findFirst({
    where: {
      questId,
      category: MessageCategory.Summary.Story,
    },
    orderBy: {
      createdAt: "desc",
    },
    include: {
      user: true,
    },
  });

  let messageCutOffDate = mostRecentSummary?.createdAt || new Date(0);

  // Get all messages that have been submitted since the last summary.
  const quest = await prisma.quest.findFirst({
    where: {
      id: questId,
      members: {
        some: {
          id: userId,
        },
      },
    },
    include: {
      members: true,
      messages: {
        where: {
          category: {
            in: [
              MessageCategory.InGame.Player,
              MessageCategory.InGame.GM,
              MessageCategory.Event.Join,
              MessageCategory.Event.Leave,
            ],
          },
          createdAt: {
            gte: messageCutOffDate,
          },
        },
        orderBy: {
          createdAt: "asc",
        },
        include: {
          user: true,
        },
      },
    },
  });

  // Inclued the most recent summary in the quest.
  if (mostRecentSummary) {
    quest?.messages.unshift(mostRecentSummary);
  }

  // Include world summary in the quest.
  quest?.messages.unshift(...worldSummary);

  return quest;
}

export async function getAssistantResponse(
  questId: string,
  userId: string
): Promise<Message> {
  const quest = await getQuestSummaryForLLM(questId, userId);

  if (!quest) {
    throw new Error("Quest not found");
  }

  // TODO: Allow configuration of number of messages until summary is created.
  if (quest.messages.length > 10) {
    // NOTE: Remove the last item from the array, which is the latest message
    // from a player.  We want to use the summary generated below to respond
    // to that message so we remove it for now.
    const summaryMessages = quest.messages.slice(0, -1);
    const summaryResponse = await LLM.getSummaryResponse(summaryMessages);

    let newMessageCreatedAt: Date;
    try {
      const latestMessage = quest.messages[quest.messages.length - 1];
      newMessageCreatedAt = latestMessage
        ? latestMessage.createdAt
        : new Date(0);
      newMessageCreatedAt.setSeconds(newMessageCreatedAt.getSeconds() - 1);
    } catch (error) {
      console.error(
        "Error while getting latest message created_at date: ",
        error
      );
      throw error;
    }

    try {
      await prisma.message.create({
        data: {
          content: summaryResponse.gmResponse,
          category: MessageCategory.Summary.Story,
          createdAt: newMessageCreatedAt,
          quest: {
            connect: {
              id: questId,
            },
          },
        },
      });
    } catch (error) {
      console.error("Error while creating new summary message: ", error);
      throw error;
    }

    return await getAssistantResponse(questId, userId);
  }

  const responseContent = await LLM.getResponse(quest.messages);

  if (!responseContent) {
    throw new Error("Invalid response from OpenAI");
  }

  const newAssistantMessage = await prisma.message.create({
    data: {
      content: responseContent.gmResponse,
      category: MessageCategory.InGame.GM,
      quest: {
        connect: {
          id: questId,
        },
      },
    },
  });

  return newAssistantMessage;
}

export async function getUserByEmail(email: string) {
  const user = await prisma.user.findFirst({
    where: {
      email,
    },
  });
  if (!user) {
    throw new Error("User with email not found");
  }
  return user;
}

export async function joinQuest(userId: string, questId: string) {
  const quest = await prisma.quest.update({
    where: {
      id: questId,
    },
    data: {
      members: {
        connect: {
          id: userId,
        },
      },
      messages: {
        create: {
          content: "joined the quest",
          category: MessageCategory.Event.Join,
          user: {
            connect: {
              id: userId,
            },
          },
        },
      },
    },
  });
  return quest;
}

export async function retconToMessage(questId: string, messageId: string) {
  const message = await prisma.message.findFirst({
    where: {
      id: messageId,
      questId,
    },
  });

  if (!message) {
    throw new Error("Message not found");
  }

  // Get all messages since given message.
  const messages = await prisma.message.findMany({
    where: {
      questId,
      createdAt: {
        gte: message.createdAt,
      },
    },
  });

  // Delete all messages since given message.
  await prisma.message.deleteMany({
    where: {
      questId,
      createdAt: {
        gte: message.createdAt,
      },
    },
  });

  await prisma.message.create({
    data: {
      content: "retconned the story",
      category: MessageCategory.Event.Retcon,
      quest: {
        connect: {
          id: questId,
        },
      },
    },
  });

  return messages;
}

export async function inviteUserToQuest(
  inviterUserId: string,
  inviteeUserEmail: string,
  questId: string
) {
  // Ensure the inviter is allowed to invite the invitee.
  const quest = await getUserQuest(inviterUserId, questId);

  // Ensure invitee is not already in the quest.
  const inviteeAlreadyInQuest = quest?.members.some(
    (user) => user.email === inviteeUserEmail
  );
  if (inviteeAlreadyInQuest) {
    throw new Error("User is already in quest");
  }

  // Ensure inviteee exists.
  const { id: inviteeUserId } = await getUserByEmail(inviteeUserEmail);

  return await prisma.quest.update({
    where: {
      id: questId,
    },
    data: {
      members: {
        connect: {
          id: inviteeUserId,
        },
      },
    },
  });
}

export async function deleteQuest(questId: string) {
  return await prisma.quest.delete({
    where: {
      id: questId,
    },
  });
}
