import { MessageCategory } from "~/constants";
import { prisma } from "~/db.server";

export type { User } from "@prisma/client";

export const createScenario = async ({
  title,
  content,
  userId,
}: {
  title: string;
  content: string;
  userId: string;
}) => {
  return prisma.message.create({
    data: {
      category: MessageCategory.Summary.World,
      content,
      isTemplate: true,
      title,
      userId,
    },
  });
};

export const getScenarios = async ({ userId }: { userId: string }) => {
  return await prisma.message.findMany({
    where: {
      OR: [{ userId: null }, { userId }],
      category: MessageCategory.Summary.World,
      isTemplate: true,
    },
  });
};
