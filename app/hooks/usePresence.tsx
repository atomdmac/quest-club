import type { User } from "@prisma/client";
import type { RefObject } from "react";
import { useCallback, useEffect, useState } from "react";
import { TYPING_STATUS } from "~/constants";
import type { Socket } from "socket.io-client";

// NOTE: We pass the socket instance down here because the `useSocket` hook
// doesn't appear to have access to the context here.  Not sure why...
export default function usePresence({
  inputElement,
  socket,
}: {
  inputElement: RefObject<HTMLInputElement>;
  socket: Socket | undefined;
}) {
  const [isTyping, setIsTyping] = useState(false);
  const [membersTyping, setMembersTyping] = useState<
    { name: string; id: string }[]
  >([]);

  useEffect(() => {
    let typingTimeout: NodeJS.Timeout;

    if (isTyping) {
      typingTimeout = setTimeout(() => {
        socket?.emit(TYPING_STATUS.IDLE);
        setIsTyping(false);
      }, 2000);
    }

    return () => {
      clearTimeout(typingTimeout);
    };
  }, [isTyping, socket, inputElement.current?.value]);

  const onKeyDown = useCallback(
    function onKeyDown() {
      if (!socket) return;
      if (!isTyping) {
        socket?.emit(TYPING_STATUS.ACTIVE);
      }
      setIsTyping(true);
    },
    [socket, isTyping]
  );

  useEffect(() => {
    if (!inputElement.current) return;
    inputElement?.current.addEventListener("keypress", onKeyDown);
    const current = inputElement.current;
    return () => {
      current?.removeEventListener("keypress", onKeyDown);
    };
  }, [inputElement, onKeyDown]);

  const onUserPresenceTypingStart = useCallback(
    function onUserPresenceTypingStart({ user }: { user: User }) {
      setMembersTyping([
        ...membersTyping,
        {
          name: user.name,
          id: user.id,
        },
      ]);
    },
    [membersTyping]
  );

  const onUserPresenceTypingEnd = useCallback(
    function onUserPresenceTypingEnd({ user }: { user: User }) {
      setMembersTyping(membersTyping.filter(({ id }) => id !== user.id));
    },
    [membersTyping]
  );

  useEffect(() => {
    if (!socket) return;

    socket.on(TYPING_STATUS.ACTIVE, onUserPresenceTypingStart);
    socket.on(TYPING_STATUS.IDLE, onUserPresenceTypingEnd);

    return () => {
      socket.off(TYPING_STATUS.ACTIVE);
      socket.off(TYPING_STATUS.IDLE);
    };
  }, [
    socket,
    membersTyping,
    onUserPresenceTypingStart,
    onUserPresenceTypingEnd,
  ]);

  return {
    isTyping,
    membersTyping,
  };
}
