import type { Message, User } from "@prisma/client";
import { useCallback, useEffect, useState } from "react";
import type { Socket } from "socket.io-client";
import { QUEST_MEMBERSHIP_STATUS, SOCKET_EVENTS_SERVER } from "~/constants";
import useWindowFocus from "./useWindowFocus";

export function useNotificationPermission() {
  const [permission, setPermission] = useState<string>();

  useEffect(() => {
    if ("Notification" in window) {
      if (Notification.permission === "default") {
        Notification.requestPermission().then((permission) => {
          setPermission(permission);
        });
      } else {
        setPermission(Notification.permission);
      }
    }
  }, []);

  return permission;
}

export function useNotifications() {
  const permission = useNotificationPermission();
  const isFocused = useWindowFocus();

  const showNotification = useCallback(
    function showNotification(title: string, body: string) {
      if (permission === "granted" && !isFocused) {
        new Notification(title, { body });
      }
    },
    [permission, isFocused]
  );

  return { showNotification, permission };
}

export function useQuestClubNotifications({
  socket,
}: {
  socket: Socket | undefined;
}) {
  const { showNotification } = useNotifications();

  const onQuestMembershipAccepted = useCallback(
    function onQuestMembershipAccepted({ user }: { user: User }) {
      showNotification("Quest Club", `${user.name} has joined the quest!`);
    },
    [showNotification]
  );

  const onQuestMembershipLeft = useCallback(
    function onQuestMembershipLeft({ user }: { user: User }) {
      showNotification("Quest Club", `${user.name} has left the quest!`);
    },
    [showNotification]
  );

  const onNewMessage = useCallback(
    function onNewMessage(message: Message & { user: User }) {
      showNotification(
        "Quest Club",
        `${message.user?.name || "GM"}: ${message.content}`
      );
    },
    [showNotification]
  );

  const onRetconMessage = useCallback(
    function onRetconMessage(message: Message & { user: User }) {
      showNotification(
        `Quest Club: ${message.user?.name || "GM"}`,
        "A message was retconned."
      );
    },
    [showNotification]
  );

  useEffect(() => {
    if (!socket) return;
    socket.on(QUEST_MEMBERSHIP_STATUS.ACCEPTED, onQuestMembershipAccepted);
    socket.on(QUEST_MEMBERSHIP_STATUS.LEFT, onQuestMembershipLeft);
    socket.on(SOCKET_EVENTS_SERVER.NEW_MESSAGE, onNewMessage);
    socket.on(SOCKET_EVENTS_SERVER.RETCON_MESSAGE, onRetconMessage);

    return () => {
      socket.off(QUEST_MEMBERSHIP_STATUS.ACCEPTED, onQuestMembershipAccepted);
      socket.off(QUEST_MEMBERSHIP_STATUS.LEFT, onQuestMembershipLeft);
      socket.off(SOCKET_EVENTS_SERVER.NEW_MESSAGE, onNewMessage);
      socket.off(SOCKET_EVENTS_SERVER.RETCON_MESSAGE, onRetconMessage);
    };
  }, [
    socket,
    showNotification,
    onQuestMembershipAccepted,
    onQuestMembershipLeft,
    onNewMessage,
    onRetconMessage,
  ]);
}
