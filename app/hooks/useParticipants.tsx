import type { User } from "@prisma/client";
import type { Socket } from "socket.io-client";
import { useCallback, useEffect, useState } from "react";
import { QUEST_MEMBERSHIP_STATUS } from "~/constants";

export type ParticipantUser = {
  id: string;
  name: string;
};

export type Participant = ParticipantUser & {
  isOwner?: boolean;
};

export function convertUserToParticipant(
  user: ParticipantUser,
  questOwnerId?: string
): Participant {
  return {
    id: user.id,
    name: user.name,
    isOwner: user.id === questOwnerId,
  };
}

export default function useParticipants({
  socket,
  initialParticipants,
  questOwnerId,
}: {
  socket: Socket | undefined;
  initialParticipants: ParticipantUser[];
  questOwnerId?: string;
}) {
  const [participants, setParticipants] = useState<Participant[]>(
    initialParticipants.map((p) => convertUserToParticipant(p, questOwnerId))
  );

  const onQuestMembershipAccepted = useCallback(
    function onQuestMembershipAccepted({ user }: { user: User }) {
      setParticipants((participants) => [
        ...participants,
        convertUserToParticipant(user, questOwnerId),
      ]);
    },
    [setParticipants, questOwnerId]
  );

  const onQuestMembershipLeft = useCallback(
    function onQuestMembershipLeft({ user }: { user: User }) {
      setParticipants((participants) =>
        participants.filter((p) => p.id !== user.id)
      );
    },
    [setParticipants]
  );

  useEffect(() => {
    if (!socket) return;
    socket.on(QUEST_MEMBERSHIP_STATUS.ACCEPTED, onQuestMembershipAccepted);
    socket.on(QUEST_MEMBERSHIP_STATUS.LEFT, onQuestMembershipLeft);

    return () => {
      socket.off(QUEST_MEMBERSHIP_STATUS.ACCEPTED, onQuestMembershipAccepted);
      socket.off(QUEST_MEMBERSHIP_STATUS.LEFT, onQuestMembershipLeft);
    };
  }, [socket, onQuestMembershipAccepted, onQuestMembershipLeft]);

  return { participants };
}
