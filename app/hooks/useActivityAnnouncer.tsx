import type { Socket } from "socket.io-client";
import { useEffect } from "react";
import type { User } from "@prisma/client";
import { PRESENCE_STATUS } from "~/constants";
import useConnectionStatus from "./useConnectionStatus";

export default function useActivityAnnouncer({
  questId,
  socket,
  user,
}: {
  questId: string | undefined;
  socket: Socket | undefined;
  user: User;
}) {
  const { isConnected } = useConnectionStatus({ socket });
  useEffect(() => {
    if (!socket || !user || !questId) {
      return;
    }

    if (!isConnected) {
      return;
    }

    if (!socket || !user) return;

    console.log("Now active in topic: ", questId, user);

    socket.emit(PRESENCE_STATUS.ACTIVE, {
      topicId: questId,
      user,
    });
  }, [isConnected, socket, user, questId]);
}
