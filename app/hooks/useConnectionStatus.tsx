import { useCallback, useEffect, useState } from "react";
import type { Socket } from "socket.io-client";

export default function useConnectionStatus({
  socket,
}: {
  socket: Socket | undefined;
}) {
  const [isConnected, setIsConnected] = useState(socket?.connected);
  const onConnect = useCallback(function onConnect() {
    setIsConnected(true);
  }, []);

  const onDisconnect = useCallback(function onDisconnect() {
    setIsConnected(false);
  }, []);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
    }
  }, [socket, socket?.connected, onConnect, onDisconnect]);

  return {
    isConnected,
  };
}
