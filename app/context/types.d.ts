import type { QuestClubPubSub } from "server/QuestClubPubSub";
import type { ActionArgs, LoaderArgs } from "@remix-run/node";

type QuestClubContext = {
  pubsub: QuestClubPubSub;
};

type QuestClubActionArgs = ActionArgs & {
  context: QuestClubContext;
};

type QuestClubLoaderArgs = LoaderArgs & {
  context: QuestClubContext;
};
