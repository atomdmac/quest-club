export type AssistantErrorType = "TOO_MANY_REQUESTS" | "UNKNOWN";

/**
 * Defines the error that will be returned to the caller of the assistant if a
 * failure occurs while attempting to get a response from the language model.
 */
export class AssistantError extends Error {
  type: AssistantErrorType;

  constructor(message: string, { type }: { type: AssistantErrorType }) {
    super(message);
    this.name = "AssistantError";
    this.type = type;
  }
}

/**
 * Maps HTTP status codes to AssistantErrorType.
 */
export const StatusToAssistantErrorsMap: Record<number, AssistantErrorType> = {
  429: "TOO_MANY_REQUESTS",
};

