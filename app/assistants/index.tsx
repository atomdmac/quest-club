import type { AssistantAdapter } from "./types";
import Mistral from "./mistral";
import OpenAi from "./openai";

function getAdapater(): AssistantAdapter {
  switch (process.env.ASSISTANT_ADAPTER) {
    case "mistral":
      return Mistral;
    case "openai":
      return OpenAi;
    default:
      throw new Error(
        `Unknown assistant adapter: ${process.env.ASSISTANT_ADAPTER}`
      );
  }
}

const adapter: AssistantAdapter = {
  getResponse: async (messages) => getAdapater().getResponse(messages),
  getSummaryResponse: async (messages) =>
    getAdapater().getSummaryResponse(messages),
};

export default adapter;
