import axios from "axios";
import type { AxiosResponse } from "axios";
import getLogger from "~/logging.server";
import { MessageCategory } from "~/constants";
import type {
  AssistantAdapter,
  AssistantResponse,
  OutgoingMessage,
} from "~/assistants/types";
import instructions from "~/assistants/mistral/instructions";
import summaryInstructions from "~/assistants/mistral/summary-instructions";
import {
  AssistantError,
  StatusToAssistantErrorsMap,
} from "~/assistants/errors";

const HOST = "https://api.mistral.ai";
const BASE_PATH = "/v1";

type MistralCreateChatCompletionResponse = {
  choices: {
    index: number;
    finish_reason: string;
    message: {
      role: string;
      content: string;
    };
  }[];
  created: number;
  id: string;
  model: string;
  object: string;
};

type MistralCreateChatCompletionRequestMessage = {
  role: string;
  content: string;
};

type MistralCreateChatCompletionRequest = {
  model: string;
  messages: MistralCreateChatCompletionRequestMessage[];
  temperature: number;
  max_tokens: number;
};

async function postRequest(
  data: any
): Promise<AxiosResponse<MistralCreateChatCompletionResponse>> {
  const response = await axios.post<MistralCreateChatCompletionResponse>(
    `${HOST}${BASE_PATH}/chat/completions`,
    data,
    {
      headers: {
        Authorization: `Bearer ${process.env.MISTRAL_API_KEY}`,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
}

/**
 * Given a list of messages, return a summary that incorporates as much relevant
 * information as possible while using the fewest number of words.  This is
 * necessary to reduce context size as the game progresses.
 */
export async function getSummaryResponse(
  messages: OutgoingMessage[]
): ReturnType<typeof getResponse> {
  messages.push({
    user: null,
    // NOTE: This is a hack to get around the fact that the MistralAI model
    // requires the last message to be from a "user".
    category: MessageCategory.InGame.Player,
    content: summaryInstructions,
  });

  console.log('GETTING A SUMMARY!');

  const gmResponse = await getResponse(messages, { prependInstructions: false });
  return {
    gmResponse: `Story Summary: ${gmResponse.gmResponse}

Use the following rules to determine how to respond:
* Jump directly into the story/action.  Do not provide any content that does not partain to the current world and scenario.  THIS IS IMPORTANT.
* DO NOT respond with a GM greeting.
* DO NOT respond with an introduction to the game.
* DO NOT respond by introducing yourself.
* DO NOT summarize the story or current scenario.
* DO NOT include any of the instructions given to you.
* DO NOT ask players to describe their characters.
* DO NOT indicate that a summary was provided in your response.
`
  };
}

/**
 * Given a list of messages, return a response from the language model.
 */
export async function getResponse(
  messages: OutgoingMessage[],
  options: { prependInstructions?: boolean } = {
    prependInstructions: true,
  }
): Promise<AssistantResponse> {
  const logger = getLogger();

  const llmMessages = convertMessagesToChatCompletionRequest(messages);

  if (options.prependInstructions === true) {
    instructions.forEach((instruction) => {
      llmMessages.unshift({
        role: "system",
        content: instruction,
      });
    });
  }

  // HACK: This is a hack to get around the fact that the MistralAI model
  // requires the last message to be from a "user".
  if (llmMessages[llmMessages.length - 1].role !== "user") {
    llmMessages.push({
      role: "user",
      content: "",
    });
  }

  const request = {
    model: "mistral-small",
    messages: llmMessages,
    temperature: 0.7,
    top_p: 1,
    max_tokens: 400,
  };

  logger.info("Sending: ", request);
  logger.info("Payload Length: ", JSON.stringify(request).length);

  // DEBUG
  // if (Math.random() > 0) {
  //   return {
  //     gmResponse: "This is a test response.",
  //   };
  // }

  try {
    const response = await postRequest(request);
    if (response.data) {
      logger.info("Response: ", JSON.stringify(response.data, null, 2));
    } else {
      logger.warn("Response has no body!");
    }

    return {
      gmResponse: await getGmResponseFromResponse(response.data),
    };
  } catch (llmError) {
    const { response } = llmError as any;

    const knownError = StatusToAssistantErrorsMap[response?.status];

    // Return error to the caller so it can be presented to the user.
    if (knownError) {
      logger.error("Mistal Error: ", {
        status: response.status,
      });

      throw new AssistantError(response.statusText, {
        type: knownError,
      });
    }

    // Unknown error, log it and return undefined.
    else {
      logger.error(
        "MistralAI: Error of unknown type - Raw response: ",
        response
      );
      logger.error(llmError);
      throw new AssistantError(response?.statusText || "Unknown", {
        type: "UNKNOWN",
      });
    }
  }
}

function convertMessageCategoryToRole(
  category: string
): "assistant" | "user" | "system" {
  switch (category) {
    case MessageCategory.Summary.Character:
    case MessageCategory.Summary.Story:
    case MessageCategory.Summary.World:
      return "system";
    case MessageCategory.InGame.Player:
      return "user";
    case MessageCategory.InGame.GM:
      return "assistant";
    default:
      return "system";
  }
}

export function convertMessagesToChatCompletionRequest(
  messages: OutgoingMessage[]
): MistralCreateChatCompletionRequestMessage[] {
  const sendableMessageCategories = [
    MessageCategory.InGame.Player,
    MessageCategory.InGame.GM,
    MessageCategory.Summary.Character,
    MessageCategory.Summary.Story,
    MessageCategory.Summary.World,
    MessageCategory.System.Instructions,
  ];
  return messages
    .filter((message) => sendableMessageCategories.includes(message.category))
    .map((message) => {
      const role = convertMessageCategoryToRole(message.category);
      const playerName =
        role !== "system" && message.user?.name ? `${message.user.name}: ` : "";
      return {
        role,
        content: `${playerName}${message.content}`,
      };
    });
}

async function getGmResponseFromResponse(
  createChatCompletionResponse: MistralCreateChatCompletionResponse
) {
  if (!createChatCompletionResponse) {
    throw new Error("No response data!");
  }

  const gmResponse = createChatCompletionResponse.choices.find(
    (choice) => choice.index === 0 && choice.finish_reason === "stop"
  );

  if (!gmResponse?.message) {
    throw new Error("No GM response!");
  }

  return gmResponse.message.content;
}

// IDEA: getCharacter(playerName: string): Character
const MistralAIAssistant: AssistantAdapter = {
  getResponse,
  getSummaryResponse,
};

export default MistralAIAssistant;
