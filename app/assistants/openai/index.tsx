import type {
  ChatCompletionRequestMessage,
  ChatCompletionRequestMessageRoleEnum,
  CreateChatCompletionResponse,
} from "openai";
import { Configuration, OpenAIApi } from "openai";
import getLogger from "~/logging.server";
import { MessageCategory } from "~/constants";
import type { AssistantAdapter, OutgoingMessage } from "~/assistants/types";
import instructions from "~/assistants/openai/instructions";
import summaryInstructions from "~/assistants/openai/summary-instructions";

export type AssistantErrorType = "TOO_MANY_REQUESTS" | "UNKNOWN";

/**
 * Defines the error that will be returned to the caller of the assistant if a
 * failure occurs while attempting to get a response from the language model.
 */
export class AssistantError extends Error {
  type: AssistantErrorType;

  constructor(message: string, { type }: { type: AssistantErrorType }) {
    super(message);
    this.name = "AssistantError";
    this.type = type;
  }
}

/**
 * Maps HTTP status codes to AssistantErrorType.
 */
export const StatusToAssistantErrorsMap: Record<number, AssistantErrorType> = {
  429: "TOO_MANY_REQUESTS",
};

/**
 * Defines the object that will be returned to the caller of the assistant.
 */
export type AssistantResponse = {
  gmResponse: string;
};

/**
 * Given a list of messages, return a summary that incorporates as much relevant
 * information as possible while using the fewest number of words.  This is
 * necessary to reduce context size as the game progresses.
 */
export async function getSummaryResponse(
  messages: OutgoingMessage[]
): ReturnType<typeof getResponse> {
  messages.push({
    user: null,
    category: MessageCategory.System.Instructions,
    content: summaryInstructions,
  });

  return await getResponse(messages);
}

/**
 * Given a list of messages, return a response from the language model.
 */
export async function getResponse(
  messages: OutgoingMessage[],
  options: { appendInstructions?: boolean } = {
    appendInstructions: true,
  }
): Promise<AssistantResponse> {
  const logger = getLogger();

  const llmMessages = convertMessagesToChatCompletionRequest(messages);

  if (options.appendInstructions) {
    llmMessages.unshift({
      role: "system",
      content: instructions,
    });
  }

  const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
  });
  const openai = new OpenAIApi(configuration);
  const request = {
    model: "gpt-4",
    messages: llmMessages,
    temperature: 0,
    max_tokens: 1000,
  };

  logger.info("Sending: ", request);
  logger.info("Payload Length: ", JSON.stringify(request).length);

  // DEBUG
  // if (Math.random() > 0) {
  //   return {
  //     gmResponse: "This is a test response.",
  //   };
  // }

  try {
    const response = await openai.createChatCompletion(request);
    if (response.data) {
      logger.info("Response: ", JSON.stringify(response.data, null, 2));
    } else {
      logger.warn("Response has no body!");
    }

    return {
      gmResponse: await getGmResponseFromResponse(response.data),
    };
  } catch (openAiError) {
    const { response } = openAiError as any;

    const knownError = StatusToAssistantErrorsMap[response?.status];

    // Return error to the caller so it can be presented to the user.
    if (knownError) {
      logger.error("Open AI Error: ", {
        status: response.status,
      });

      throw new AssistantError(response.statusText, {
        type: knownError,
      });
    }

    // Unknown error, log it and return undefined.
    else {
      logger.error("Open AI Error: unknown type - Raw response: ", response);
      throw new AssistantError(response?.statusText || "Unknown", {
        type: "UNKNOWN",
      });
    }
  }
}

function convertMessageCategoryToRole(
  category: string
): ChatCompletionRequestMessageRoleEnum {
  switch (category) {
    case MessageCategory.Summary.Character:
    case MessageCategory.Summary.Story:
    case MessageCategory.Summary.World:
      return "system";
    case MessageCategory.InGame.Player:
      return "user";
    case MessageCategory.InGame.GM:
      return "assistant";
    default:
      return "system";
  }
}

export function convertMessagesToChatCompletionRequest(
  messages: OutgoingMessage[]
): ChatCompletionRequestMessage[] {
  const sendableMessageCategories = [
    MessageCategory.InGame.Player,
    MessageCategory.InGame.GM,
    MessageCategory.Summary.Character,
    MessageCategory.Summary.Story,
    MessageCategory.Summary.World,
    MessageCategory.System.Instructions,
  ];
  return messages
    .filter((message) => sendableMessageCategories.includes(message.category))
    .map((message) => {
      const role = convertMessageCategoryToRole(message.category);
      const playerName =
        role !== "system" && message.user?.name ? `${message.user.name}: ` : "";
      return {
        role,
        content: `${playerName}${message.content}`,
      };
    });
}

async function getGmResponseFromResponse(
  createChatCompletionResponse: CreateChatCompletionResponse
) {
  if (!createChatCompletionResponse) {
    throw new Error("No response data!");
  }

  const gmResponse = createChatCompletionResponse.choices.find(
    (choice) => choice.index === 0 && choice.finish_reason === "stop"
  );

  if (!gmResponse?.message) {
    throw new Error("No GM response!");
  }

  return gmResponse.message.content;
}

// IDEA: getCharacter(playerName: string): Character
const OpenAIAssistant: AssistantAdapter = {
  getResponse,
  getSummaryResponse
};

export default OpenAIAssistant;
