import type { User } from "@prisma/client";

/**
 * Defines the object that will be returned to the caller of the assistant.
 */
export type AssistantResponse = {
  gmResponse: string;
};

export type OutgoingMessage = {
  category: string;
  content: string;
  user: User | null;
};

export type AssistantAdapter = {
  /**
   * Returns a summary of the conversation up to the latest message.
   */
  getSummaryResponse: (
    messages: OutgoingMessage[]
  ) => Promise<AssistantResponse>;
  /**
   * Returns a response to the latest messages.
   */
  getResponse: (
    messages: OutgoingMessage[],
    options?: {
      prependInstructions?: boolean;
    }
  ) => Promise<AssistantResponse>;
};
