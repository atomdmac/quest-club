// Events originating from the Server
export enum SOCKET_EVENTS_SERVER {
  ASSISTANT_TYPING_END = "assistant_typing_end",
  ASSISTANT_TYPING_START = "assistant_typing_start",
  CONNECT = "connect",
  DISCONNECT = "disconnect",
  NEW_MESSAGE = "new_message",
  RETCON_MESSAGE = "retcon_message",
  PONG = "pong",
}

export enum HEALTH_CHECK {
  PING = "ping",
  PONG = "pong",
}

// Used by the client to request quest membership changes.
// TODO: Do we really need this?  Can't we just use the endpoints?
export enum QUEST_MEMBERSHIP_STATUS_REQUEST {
  JOIN = "quest_membership_status_request_join",
  LEAVE = "quest_membership_status_request_leave",
}

// Events regarding changes in quest memberships.  i.e. When someone is added
// or leaves from a quest.  This occurs once when the user is invited to a quest
// and joins.  It occurs again when the user renounces their membership in the quest.
export enum QUEST_MEMBERSHIP_STATUS {
  ACCEPTED = "quest_membership_accepted",
  LEFT = "quest_membership_left",
}

// Events regarding wheather a user is currently actively particiapting in a
// quest or if they're away from their keyboard.
export enum PRESENCE_STATUS {
  ACTIVE = "presence_status_active",
  IDLE = "presence_status_idle",
}

// Events regarding wheather a user is currently typing or not.
export enum TYPING_STATUS {
  ACTIVE = "typing_status_active",
  IDLE = "typing_status_idle",
}

export const MessageCategory = {
  System: {
    Instructions: "system-instructions",
  },
  Summary: {
    Character: "summary-character",
    World: "summary-world",
    Story: "summary-story",
  },
  Event: {
    Join: "event-join",
    Leave: "event-leave",
    Retcon: "event-retcon",
  },
  InGame: {
    GM: "gm-in-game",
    Player: "player-in-game",
  },
  Meta: {
    Player: "meta-player",
  }
}
