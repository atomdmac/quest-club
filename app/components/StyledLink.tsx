import { Link } from "@remix-run/react";
import type { LinkProps } from "@remix-run/react";
import type { ButtonHTMLAttributes } from "react";

export default function StyledLink({
  children,
  className,
  ...rest
}: LinkProps) {
  return (
    <Link {...rest} className={`${className || ""} underline`}>
      {children}
    </Link>
  );
}

export function StyledLinkButton({
  children,
  className,
  ...rest
}: ButtonHTMLAttributes<HTMLButtonElement>) {
  return (
    <button className={`${className || ""} underline`} {...rest}>
      {children}
    </button>
  );
}
