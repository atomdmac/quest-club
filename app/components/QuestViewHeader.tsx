import StyledLink, { StyledLinkButton } from "./StyledLink";
import { GetInviteLinkButton } from "./InviteUserToQuestForm";
import { useState } from "react";

type Participant = {
  id: string;
  name: string;
  isOwner?: boolean;
};

export default function QuestViewHeader({
  children,
  questId,
  questTitle,
  participants,
  summary,
  canEdit,
  canInvite,
}: {
  questId: string;
  questTitle: string;
  participants: Participant[];
  summary: string;
  canEdit: boolean;
  children?: React.ReactNode;
  canInvite?: boolean;
}) {
  const formattedParticipants = participants.map((p) => (
    <span
      key={p.id}
      className={`${
        p.isOwner && "mr-1 font-bold"
      } after:content-[','] last:after:content-none`}
    >
      {p.name}
    </span>
  ));

  const [showDetails, setShowDetails] = useState(false);

  function toggleShowDetails() {
    setShowDetails(!showDetails);
  }

  return (
    <div className="sticky top-0">
      <div className="bg-slate-800 py-4">
        <div className="flex items-center justify-between">
          <h1 className="mr-4 pt-4 text-2xl font-bold">{questTitle}</h1>
          <div className="flex">
            <StyledLinkButton
              className="mr-4 text-slate-500"
              onClick={toggleShowDetails}
            >
              {showDetails ? "Hide" : "Show"} Details
            </StyledLinkButton>
            {canInvite && <GetInviteLinkButton className="mr-4" />}
            {canEdit && (
              <StyledLink className="mr-4 text-slate-500" to="edit">
                Edit
              </StyledLink>
            )}
            <StyledLink className="text-slate-500" to="/quests/">
              Back to Quests
            </StyledLink>
          </div>
        </div>
        {showDetails && (
          <div className="pb-2">
            <div className="my-1 text-slate-500">
              Participants: {formattedParticipants}
            </div>
            <h1>Summary</h1>
            <span className="text-slate-300">{summary}</span>
          </div>
        )}
        {children}
      </div>
      <div className="h-4 border-t border-t-slate-300 pt-4 bg-gradient-to-b from-slate-100 from-0% dark:from-slate-800" />
    </div>
  );
}
