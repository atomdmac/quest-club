import { Form, useParams } from "@remix-run/react";
import { useEffect, useState } from "react";
import Input from "~/components/Input";
import Button from "./Button";
import { StyledLinkButton } from "./StyledLink";

export default function InviteUserToQuestForm() {
  const { questId } = useParams<{ questId: string }>();
  const [showForm, setShowForm] = useState(false);

  const form = (
    <Form method="post" action={`/quests/invite/${questId}`} className="flex">
      <Input
        type="text"
        name="inviteeUserEmail"
        className="w-full rounded border border-slate-500 px-2 py-1 text-lg"
      />
      <Button type="submit" className="ml-2">
        Invite
      </Button>
      <Button className="ml-2" onClick={() => setShowForm(false)}>
        Cancel
      </Button>
    </Form>
  );

  const revealButton = (
    <StyledLinkButton
      className="text-slate-500"
      onClick={() => setShowForm(true)}
    >
      Invite User
    </StyledLinkButton>
  );

  return <div>{showForm ? form : revealButton}</div>;
}

export function GetInviteLinkButton({ className }: { className: string }) {
  const [showCopiedMessage, setShowCopiedMessage] = useState(false);
  const { questId } = useParams<{ questId: string }>();

  function copyToClipboard() {
    navigator.clipboard.writeText(
      `${window.location.origin}/quests/${questId}/join`
    );
    setShowCopiedMessage(true);
  }

  useEffect(() => {
    if (showCopiedMessage) {
      const timeout = setTimeout(() => {
        setShowCopiedMessage(false);
      }, 2000);
      return () => {
        setShowCopiedMessage(false);
        clearTimeout(timeout);
      };
    }
  }, [showCopiedMessage]);

  const textColorClass = showCopiedMessage ? "text-green-500" : "text-slate-500";

  return (
    <div className={className}>
      <StyledLinkButton className={textColorClass} onClick={copyToClipboard}>
        {showCopiedMessage ? "Copied!" : "Invite"}
      </StyledLinkButton>
    </div>
  );
}
