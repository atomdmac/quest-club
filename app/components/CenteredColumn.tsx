export default function CenteredColumn({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="relative flex w-screen flex-col px-4 lg:max-w-screen-lg">
      {children}
    </div>
  );
}
