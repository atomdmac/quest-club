import { Form } from "@remix-run/react";
import { useRef, useEffect } from "react";
import Input from "~/components/Input";
import Button from "~/components/Button";

const DefaultSubmitButton = (
  <Button type="submit" className="w-full bg-blue-300 dark:bg-slate-200">
    Update
  </Button>
);

export default function Account({
  playerName,
  email,
  errors,
  children = DefaultSubmitButton,
}: {
  playerName?: string;
  email?: string;
  errors?: { [key: string]: string | null };
  children?: React.ReactNode;
}) {
  const playerNameRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (errors?.playerName) {
      playerNameRef.current?.focus();
    } else if (errors?.email) {
      emailRef.current?.focus();
    } else if (errors?.password) {
      passwordRef.current?.focus();
    }
  }, [errors]);

  return (
    <Form method="post" className="space-y-6">
      <div>
        <label
          htmlFor="email"
          className="block text-sm font-medium text-slate-50"
        >
          Name
        </label>
        <div className="mt-1">
          <Input
            ref={playerNameRef}
            id="playerName"
            required
            autoFocus={true}
            name="playerName"
            type="text"
            defaultValue={playerName}
            aria-invalid={errors?.playerName ? true : undefined}
            aria-describedby="player-name-error"
            className="w-full rounded border border-gray-500 px-2 py-1 text-lg"
          />
          {errors?.playerName ? (
            <div className="pt-1 text-red-700" id="email-error">
              {errors.playerName}
            </div>
          ) : null}
        </div>
      </div>
      <div>
        <label
          htmlFor="email"
          className="block text-sm font-medium text-slate-50"
        >
          Email address
        </label>
        <div className="mt-1">
          <Input
            ref={emailRef}
            id="email"
            required
            autoFocus={true}
            name="email"
            type="email"
            autoComplete="email"
            aria-invalid={errors?.email ? true : undefined}
            aria-describedby="email-error"
            className="w-full rounded border border-gray-500 px-2 py-1 text-lg"
            defaultValue={email}
          />
          {errors?.email ? (
            <div className="pt-1 text-red-700" id="email-error">
              {errors.email}
            </div>
          ) : null}
        </div>
      </div>

      <div>
        <label
          htmlFor="password"
          className="block text-sm font-medium text-slate-50"
        >
          Password
        </label>
        <div className="mt-1">
          <Input
            id="password"
            ref={passwordRef}
            name="password"
            type="password"
            autoComplete="new-password"
            aria-invalid={errors?.password ? true : undefined}
            aria-describedby="password-error"
          />
          {errors?.password ? (
            <div className="pt-1 text-red-700" id="password-error">
              {errors.password}
            </div>
          ) : null}
        </div>
      </div>
      {children}
    </Form>
  );
}
