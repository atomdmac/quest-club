export default function Heading({ children }: { children: React.ReactNode }) {
  return (
    <h1 className="mb-4 border-b pb-2 pt-14 text-2xl font-bold">{children}</h1>
  );
}
