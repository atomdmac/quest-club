import type { AllHTMLAttributes } from "react";
import { useRouteError } from "@remix-run/react";
import { isRouteErrorResponse } from "@remix-run/react";

export default function ErrorToast(props: AllHTMLAttributes<HTMLDivElement>) {
  const endPointState = useRouteError();
  if (!isRouteErrorResponse(endPointState)) return null;
  return (
    <header
      className={`mt-2 rounded bg-pink-500 p-2 text-pink-950 ${props.className}`}
    >
      Error: {endPointState.statusText}
    </header>
  );
}
