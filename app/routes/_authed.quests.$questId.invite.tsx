import type { ActionArgs, V2_MetaFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { inviteUserToQuest } from "~/models/quest.server";
import { requireUserId } from "~/session.server";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const action = async ({ request, params }: ActionArgs) => {
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const userId = await requireUserId(request);
  const formData = await request.formData();
  const inviteeUserEmail = formData.get("inviteeUserEmail") as string;
  if (!inviteeUserEmail) throw new Error("Missing invitee email");
  await inviteUserToQuest(userId, inviteeUserEmail, questId);
  return redirect(request.headers.get("Referer") || "/");
};
