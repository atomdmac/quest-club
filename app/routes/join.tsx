import type { ActionArgs, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useActionData, useSearchParams } from "@remix-run/react";
import { useEffect, useRef } from "react";
import QuestClubLogo from "~/components/QuestClubLogo";

import { createUser, getUserByEmail } from "~/models/user.server";
import { createUserSession, getUserId } from "~/session.server";
import { createFormValidationFunction, safeRedirect, validateEmail } from "~/utils";
import Button from "~/components/Button";
import Account from "~/components/Account";

export const loader = async ({ request }: LoaderArgs) => {
  const userId = await getUserId(request);
  if (userId) return redirect("/");
  return json({});
};

const getValidationErrors = createFormValidationFunction({
  playerName: "",
  email: "",
  password: "",
  redirectTo: "",
});

export const action = async ({ request }: ActionArgs) => {
  const formData = await request.formData();
  const playerName = formData.get("playerName");
  const email = formData.get("email");
  const password = formData.get("password");
  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");

  if (typeof playerName !== "string" || playerName.length === 0) {
    return json(getValidationErrors("playerName", "Player name is invalid"), {
      status: 400,
    });
  }

  if (!validateEmail(email)) {
    return json(getValidationErrors("email", "Email is invalid"), {
      status: 400,
    });
  }

  if (typeof password !== "string" || password.length === 0) {
    return json(getValidationErrors("password", "Password is required"), {
      status: 400,
    });
  }

  if (password.length < 8) {
    return json(getValidationErrors("password", "Password is too short"), {
      status: 400,
    });
  }

  const existingUser = await getUserByEmail(email);
  if (existingUser) {
    return json(getValidationErrors("email", "Email is already in use"), {
      status: 400,
    });
  }

  const user = await createUser(playerName, email, password);

  return createUserSession({
    redirectTo,
    remember: false,
    request,
    userId: user.id,
  });
};

export const meta: V2_MetaFunction = () => [{ title: "Sign Up" }];

export default function Join() {
  const [searchParams] = useSearchParams();
  const redirectTo = searchParams.get("redirectTo") ?? undefined;
  const actionData = useActionData<typeof action>();
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (actionData?.errors?.email) {
      emailRef.current?.focus();
    } else if (actionData?.errors?.password) {
      passwordRef.current?.focus();
    }
  }, [actionData]);

  return (
    <div className="flex min-h-full flex-col justify-center">
      <div className="mx-auto w-full max-w-md px-8">
        <QuestClubLogo />
        <Account errors={actionData?.errors}>
          <input type="hidden" name="redirectTo" value={redirectTo} />
          <Button
            type="submit"
            className="w-full bg-blue-300 dark:bg-slate-200"
          >
            Join the Club
          </Button>
        </Account>
      </div>
    </div>
  );
}
