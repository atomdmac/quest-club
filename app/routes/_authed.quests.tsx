import type { V2_MetaFunction } from "@remix-run/node";
import { Link, Outlet } from "@remix-run/react";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export default function Quests() {
  return (
    <main className="flex min-h-full flex-col items-center">
      <Outlet />
    </main>
  );
}
