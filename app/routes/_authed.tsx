import type { V2_MetaFunction } from "@remix-run/node";
import { Form, Outlet, NavLink } from "@remix-run/react";
import { useEffect, useState } from "react";
import { io } from "socket.io-client";
import type { Socket } from "socket.io-client";
import { SocketProvider } from "~/context/socket";
import { useUser } from "~/utils";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

function AuthedHeader({ firstName }: { firstName: string }) {
  return (
    <nav className="auth-header flex justify-center bg-slate-400 dark:bg-black">
      <div className="px-4 flex w-full justify-between lg:max-w-screen-lg">
        <p className="pt-6">Hail, {firstName}!</p>
        <span className="flex grow justify-end">
          <NavLink className="nav-link p-2 pt-6 mr-3" to="/quests">
            Quests
          </NavLink>
          <NavLink className="nav-link p-2 pt-6 mr-10" to="/account">
            Account
          </NavLink>
          <Form method="post" action="/logout">
            <button className="p-2 pt-6 text-slate-500" type="submit">Logout</button>
          </Form>
        </span>
      </div>
    </nav>
  );
}

function SocketWrapper() {
  // Ensure user is authenticated.
  useUser();
  const [socket, setSocket] = useState<Socket>();

  useEffect(() => {
    const socket = io();
    setSocket(socket);
    return () => {
      socket.close();
    };
  }, []);

  return (
    <SocketProvider socket={socket}>
      <Outlet />
    </SocketProvider>
  );
}

export default function Auth() {
  const user = useUser();

  return (
    <div>
      <AuthedHeader firstName={user.name} />
      <SocketWrapper />
    </div>
  );
}
