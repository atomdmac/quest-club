import type { V2_MetaFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { QUEST_MEMBERSHIP_STATUS } from "~/constants";
import type { QuestClubLoaderArgs } from "~/context/types";
import { joinQuest } from "~/models/quest.server";
import { requireUser, requireUserId } from "~/session.server";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const loader = async ({
  context,
  request,
  params,
}: QuestClubLoaderArgs) => {
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const redirectTo = new URL(request.url).pathname;
  const userId = await requireUserId(request, redirectTo);
  const user = await requireUser(request);

  await joinQuest(userId, questId);

  context.pubsub.broadcastToTopic(
    QUEST_MEMBERSHIP_STATUS.ACCEPTED,
    { user },
    questId
  );

  return redirect(`/quests/${questId}`);
};
