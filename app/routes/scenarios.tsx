import { json } from "@remix-run/node";
import type { LoaderArgs } from "@remix-run/node";
import { getScenarios } from "~/models/scenarios.server";
import { requireUserId } from "~/session.server";

export const loader = async ({ request }: LoaderArgs) => {
  const userId = await requireUserId(request);
  const scenarios = await getScenarios({ userId });
  return json(scenarios);
};
