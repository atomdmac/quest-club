import type { V2_MetaFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { useLoaderData, Form, Link } from "@remix-run/react";
import Button from "~/components/Button";
import CenteredColumn from "~/components/CenteredColumn";
import Heading from "~/components/Heading";
import Input from "~/components/Input";
import type { QuestClubLoaderArgs } from "~/context/types";
import {
  getEditableQuest,
  editQuest,
  getUserOwnsQuest,
} from "~/models/quest.server";
import { requireUserId } from "~/session.server";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const loader = async ({
  request,
  params,
}: QuestClubLoaderArgs) => {
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const userId = await requireUserId(request);
  const userCanEdit = await getUserOwnsQuest(userId, questId);
  if (!userCanEdit) {
    return json({
      quest: null,
      summary: null,
      worldSummary: null,
      errors: {
        message: "You do not have permission to edit this quest.",
      },
    });
  }
  const quest = await getEditableQuest(userId, questId);
  return json({
    ...quest,
    errors: {},
  });
};

export const action = async ({ request, params }: QuestClubLoaderArgs) => {
  const userId = await requireUserId(request);
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const formData = await request.formData();
  const title = formData.get("title") as string;
  const summary = formData.get("summary") as string;
  const worldSummary = formData.get("worldSummary") as string;
  const updatedQuest = await editQuest(
    userId,
    questId,
    title,
    worldSummary,
    summary
  );
  return redirect(`/quests/${updatedQuest.id}`);
};

export default function EditQuest() {
  const data = useLoaderData();
  const { errors, quest, summary, worldSummary } = data;
  const ErrorDisplay = () => (
    <>
      <div className="mb-4 rounded bg-red-500 p-4 text-white">
        {errors.message}
      </div>
    </>
  );

  const EditForm = () => (
    <Form method="post" className="flex w-full flex-col justify-end">
      <label
        htmlFor="title"
        className="mb-2 mt-4 inline-block font-medium text-slate-50"
      >
        Quest Title
      </label>
      <Input name="title" defaultValue={quest.title} required />
      <label
        htmlFor="summary"
        className="mb-2 mt-4 inline-block font-medium text-slate-50"
      >
      World Summary
      </label>
      <textarea
        name="worldSummary"
        className="min-h-[12em] w-full rounded border border-slate-800 bg-white p-2 text-black dark:bg-slate-500 dark:text-slate-50"
        defaultValue={worldSummary.content || "No world summary found!"}
        disabled={!worldSummary}
        required={worldSummary ? true : false}
      />
      <label
        htmlFor="summary"
        className="mb-2 mt-4 inline-block font-medium text-slate-50"
      >
        Current Summary
      </label>
      <textarea
        name="summary"
        className="min-h-[12em] w-full rounded border border-slate-800 bg-white p-2 text-black dark:bg-slate-500 dark:text-slate-50"
        defaultValue={summary?.content || "The quest has not begun yet..."}
        disabled={!summary}
        required={summary ? true : false}
      />
      <Button type="submit" className="mt-4">
        Save
      </Button>
      <Link
        className="mt-2 inline-block rounded bg-slate-500 p-2 text-center"
        relative="path"
        to=".."
      >
        Cancel
      </Link>
    </Form>
  );
  return (
    <CenteredColumn>
      <Heading>Edit Quest Details</Heading>
      {errors?.message ? <ErrorDisplay /> : <EditForm />}
    </CenteredColumn>
  );
}
