import type { ActionArgs, V2_MetaFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { deleteQuest } from "~/models/quest.server";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const action = async ({ params }: ActionArgs) => {
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  await deleteQuest(questId);
  return redirect("/quests");
};
