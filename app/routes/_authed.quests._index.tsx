import type { ActionArgs, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import {
  Form,
  useFetcher,
  useFormAction,
  useLoaderData,
  useNavigation,
} from "@remix-run/react";
import type {
  ChangeEvent,
  SelectHTMLAttributes,
} from "react";
import { useEffect, useRef, useState } from "react";
import Button from "~/components/Button";
import CenteredColumn from "~/components/CenteredColumn";
import Heading from "~/components/Heading";
import Input from "~/components/Input";
import StyledLink from "~/components/StyledLink";
import {
  createQuest,
  getAllUserQuests,
  getAssistantResponse,
} from "~/models/quest.server";
import { requireUserId } from "~/session.server";
import type { loader as scenarioLoader } from "./scenarios";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const loader = async ({ request }: LoaderArgs) => {
  const userId = await requireUserId(request);
  const quests = await getAllUserQuests(userId);
  return json(quests);
};

export const action = async ({ request }: ActionArgs) => {
  const userId = await requireUserId(request);
  const formData = await request.formData();
  const title = formData.get("title") as string;
  const content = formData.get("content") as string;
  const quest = await createQuest(userId, title, content);
  await getAssistantResponse(quest.id, userId);
  return redirect(`/quests/${quest.id}`);
};

function SelectBox({
  children,
  placeholder,
  ...rest
}: SelectHTMLAttributes<HTMLSelectElement>) {
  return (
    <select
      required
      {...rest}
      className="mb-4 rounded border border-black bg-white p-4
 px-2 py-2 text-black dark:bg-slate-500 dark:text-slate-50
      "
    >
      {placeholder && (
        <option value="" disabled selected>
          {placeholder}
        </option>
      )}
      {children}
    </select>
  );
}

function NewQuestForm() {
  const fetcher = useFetcher<typeof scenarioLoader>();
  const nav = useNavigation();
  const formAction = useFormAction();

  const [scenarios, setScenarios] = useState<
    { value: string; label: string }[]
  >([]);
  const contentRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (fetcher.data) {
      setScenarios(
        fetcher.data.map((storySummary) => {
          return {
            value: storySummary.id,
            label: storySummary.title,
          };
        })
      );
    }
  }, [setScenarios, fetcher.data]);

  useEffect(() => {
    if (fetcher.state === "idle" && fetcher.data === undefined) {
      fetcher.load("/scenarios");
    }
  }, [fetcher]);

  const onChangeStartingScenario = (e: ChangeEvent<HTMLSelectElement>) => {
    if (contentRef.current) {
      contentRef.current.value = (fetcher.data || []).reduce(
        (content: string, scenario) => {
          return scenario.id === e.target.selectedOptions[0].value
            ? scenario.content
            : content;
        },
        ""
      );
    }
  };

  const isSubmitting = !!(
    nav.state === "submitting" && nav.formAction === formAction
  );

  return (
    <div>
      <Heading>New Quest</Heading>
      <Form method="post" className="flex flex-col">
        <label htmlFor="title" className="mb-2">
          Quest Title
        </label>
        <Input
          required
          type="text"
          name="title"
          className="mb-4"
          placeholder="Quest Title"
        />
        <label htmlFor="startingScenario" className="mb-2">
          Scenario Preset
        </label>
        <SelectBox onChange={onChangeStartingScenario}>
          <option value="custom">Custom Starting Scenario</option>
          {scenarios.map((scenario) => (
            <option key={scenario.value} value={scenario.value}>
              {scenario.label}
            </option>
          ))}
        </SelectBox>
        <label htmlFor="content" className="mb-2">
          Scenario Content
        </label>
        <textarea
          ref={contentRef}
          required
          name="content"
          placeholder="Set the scene..."
          className="min-h-[10em] mb-8 rounded border border-slate-950 p-2 text-black dark:bg-slate-500 dark:text-slate-50"
        />
        <Button
          type="submit"
          disabled={isSubmitting}
          className="dark:text-white"
        >
          {isSubmitting ? "Prepping..." : "Onward!"}
        </Button>
      </Form>
    </div>
  );
}

export function QuestList({
  quests,
}: {
  quests: {
    id: string;
    title: string;
    messages: { content: string | undefined }[];
  }[];
}) {
  const questEls = quests.map((quest) => (
    <li
      key={quest.id}
      className="flex items-center p-4 odd:bg-slate-200 dark:odd:bg-slate-700"
    >
      <div className="mr-3 block border-teal-400 text-3xl font-bold text-teal-400">
        #
      </div>
      <div className="flex-grow">
        <StyledLink className="flex-grow" to={`/quests/${quest.id}`}>
          {quest.title}
        </StyledLink>
        <details>
          <summary>Summary</summary>
          {quest.messages[0]?.content || "The journey begins..."}
        </details>
      </div>
    </li>
  ));

  return questEls.length ? (
    <ul className="flex flex-col">{questEls}</ul>
  ) : (
    <p>No Quests Yet!</p>
  );
}

export default function Quests() {
  const data = useLoaderData<typeof loader>();

  return (
    <CenteredColumn>
      <Heading>Quest Log</Heading>
      <QuestList quests={data} />
      <NewQuestForm />
    </CenteredColumn>
  );
}
