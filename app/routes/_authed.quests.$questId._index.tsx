import type { V2_MetaFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Form, useFetcher, useLoaderData, useParams } from "@remix-run/react";
import { useCallback, useEffect, useRef, useState } from "react";
import Button from "~/components/Button";
import CenteredColumn from "~/components/CenteredColumn";
import ErrorToast from "~/components/ErrorMessage";
import Input from "~/components/Input";
import QuestViewHeader from "~/components/QuestViewHeader";
import { MessageCategory, SOCKET_EVENTS_SERVER } from "~/constants";
import { useSocket } from "~/context/socket";
import type { QuestClubActionArgs, QuestClubLoaderArgs } from "~/context/types";
import useActivityAnnouncer from "~/hooks/useActivityAnnouncer";
import useConnectionStatus from "~/hooks/useConnectionStatus";
import { useQuestClubNotifications } from "~/hooks/useNotifications";
import useParticipants from "~/hooks/useParticipants";
import usePresence from "~/hooks/usePresence";
import {
  assertUserIsInQuest,
  createUserMessage,
  getAssistantResponse,
  getLatestQuestSummary,
  getUserOwnsQuest,
  getUserQuest,
  retconToMessage,
} from "~/models/quest.server";
import { requireUserId } from "~/session.server";
import { useUser } from "~/utils";

export const meta: V2_MetaFunction = () => [{ title: "Quest Club" }];

export const loader = async (args: QuestClubLoaderArgs) => {
  const { request, params } = args;
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const userId = await requireUserId(request);
  let quest;
  let summary;
  try {
    quest = await getUserQuest(userId, questId);
    summary = await getLatestQuestSummary(questId);
  } catch {
    throw new Response(null, { statusText: "Quest not found", status: 404 });
  }
  if (!quest) {
    throw new Response(null, { statusText: "Quest not found", status: 404 });
  }

  return json({
    quest,
    summary,
  });
};

const COMMANDS = {
  CREATE_MESSAGE: "create-message",
  RETCON_MESSAGE: "retcon-message",
};

export const actionCreateMessage = async ({
  context,
  request,
  formData,
  userId,
  questId,
}: QuestClubActionArgs & {
  formData: FormData;
  userId: string;
  questId: string;
}) => {
  const socketId = formData.get("socketId") as string;
  const message = formData.get("message") as string;
  const isMeta = formData.get("isMeta") === "true";

  // Create user message immediately so that the user sees that the app is
  // responding to their input.
  const newUserMessage = await createUserMessage(
    userId,
    questId,
    message,
    isMeta
  );

  // If user is metagaming, don't send their message to the GM.
  if (isMeta) {
    context.pubsub.broadcastToTopic(
      SOCKET_EVENTS_SERVER.NEW_MESSAGE,
      newUserMessage,
      questId,
      socketId
    );
    return newUserMessage;
  } else {
    context.pubsub.broadcastToTopic(
      SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_START,
      {},
      questId
    );

    context.pubsub.broadcastToTopic(
      SOCKET_EVENTS_SERVER.NEW_MESSAGE,
      newUserMessage,
      questId
    );

    const newAssistantMessage = await getAssistantResponse(questId, userId);

    context.pubsub.broadcastToTopic(
      SOCKET_EVENTS_SERVER.NEW_MESSAGE,
      newAssistantMessage,
      questId
    );

    context.pubsub.broadcastToTopic(
      SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_END,
      {},
      questId
    );
    return newAssistantMessage;
  }
};

export const actionRetconMessage = async ({
  context,
  formData,
  userId,
  questId,
}: QuestClubActionArgs & {
  formData: FormData;
  userId: string;
  questId: string;
}) => {
  const socketId = formData.get("socketId") as string;
  const messageId = formData.get("messageId") as string;
  const canRetcon = await getUserOwnsQuest(userId, questId);
  if (!canRetcon) {
    return new Response(null, { statusText: "Quest not found", status: 404 });
  }

  const deletedMessages = await retconToMessage(questId, messageId);
  const messageIds = deletedMessages.map((m) => m.id);
  context.pubsub.broadcastToTopic(
    SOCKET_EVENTS_SERVER.RETCON_MESSAGE,
    {
      messageIds,
    },
    questId
  );

  return new Response(null);
};

export const action = async ({
  context,
  request,
  params,
}: QuestClubActionArgs) => {
  const { questId } = params;
  if (!questId) throw new Error("Missing quest ID");
  const userId = await requireUserId(request);
  await assertUserIsInQuest(userId, questId);
  const formData = await request.formData();
  const command =
    (formData.get("command") as string) || COMMANDS.CREATE_MESSAGE;
  switch (command) {
    case COMMANDS.CREATE_MESSAGE:
      return await actionCreateMessage({
        context,
        request,
        params,
        formData,
        userId,
        questId,
      });
    case COMMANDS.RETCON_MESSAGE:
      return await actionRetconMessage({
        context,
        request,
        params,
        formData,
        questId,
        userId,
      });
    default:
      throw new Error(`Unknown command: ${command}`);
  }
};

export function useScrollToBottomOnChange(trigger: any) {
  useEffect(() => {
    const page = document.querySelector("html");
    page?.scrollTo(0, page.scrollHeight);
  }, [trigger]);
}

export function useQuestViewSocket() {
  const loaderData = useLoaderData<typeof loader>();
  const socket = useSocket();
  const [assistantIsTyping, setAssistantIsTyping] = useState(false);
  const [messageList, setMessageList] = useState<
    typeof loaderData.quest.messages
  >([]);

  useEffect(() => {
    setMessageList(loaderData.quest.messages);
  }, [loaderData.quest.messages]);

  const onNewMessage = useCallback(
    function onNewMessage(newMessage: any) {
      setMessageList([...messageList, newMessage]);
    },
    [messageList]
  );

  const onRetconMessage = useCallback(
    function onRetconMessage({ messageIds }: { messageIds: string[] }) {
      setMessageList(messageList.filter((m) => !messageIds.includes(m.id)));
    },
    [messageList]
  );

  const onAssistantTypingStart = useCallback(function onAssistantTypingStart() {
    setAssistantIsTyping(true);
  }, []);

  const onAssistantTypingEnd = useCallback(function onAssistantTypingEnd() {
    setAssistantIsTyping(false);
  }, []);

  useEffect(() => {
    if (!socket) return;
    socket.on(SOCKET_EVENTS_SERVER.NEW_MESSAGE, onNewMessage);
    socket.on(SOCKET_EVENTS_SERVER.RETCON_MESSAGE, onRetconMessage);
    socket.on(
      SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_START,
      onAssistantTypingStart
    );
    socket.on(SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_END, onAssistantTypingEnd);
    return () => {
      socket.off(SOCKET_EVENTS_SERVER.NEW_MESSAGE, onNewMessage);
      socket.off(SOCKET_EVENTS_SERVER.RETCON_MESSAGE, onRetconMessage);
      socket.off(
        SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_START,
        onAssistantTypingStart
      );
      socket.off(
        SOCKET_EVENTS_SERVER.ASSISTANT_TYPING_END,
        onAssistantTypingEnd
      );
    };
  }, [
    messageList,
    socket,
    onNewMessage,
    onRetconMessage,
    onAssistantTypingStart,
    onAssistantTypingEnd,
  ]);

  return {
    socket,
    assistantIsTyping,
    messageList,
  };
}

function ConnectivityIndicator() {
  return (
    <div className="mt-4 flex justify-end">
      <span className="rounded bg-red-500 p-1 px-2 text-red-900">
        Reconnecting...
      </span>
    </div>
  );
}

function RetconForm({
  messageId,
  className,
}: {
  messageId: string;
  className?: string;
}) {
  const fetcher = useFetcher();
  function onClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    const userConfirmation = confirm(
      "All messages sent after this one will be deleted.\n\nOK to confirm, Cancel to cancel."
    );

    if (userConfirmation) {
      fetcher.submit({ messageId });
    } else {
      event.preventDefault();
      return;
    }
  }

  const classes = `mr-6 cursor-pointer text-sm underline text-red-500 ${className}`;
  return (
    <fetcher.Form method="post" className="flex">
      <input type="hidden" name="messageId" value={messageId} />
      <input type="hidden" name="command" value={COMMANDS.RETCON_MESSAGE} />
      <button onClick={onClick} className={classes}>
        Retcon
      </button>
    </fetcher.Form>
  );
}

export default function QuestView() {
  const socket = useSocket();
  const user = useUser();
  const loaderData = useLoaderData<typeof loader>();
  const { questId } = useParams<{ questId: string }>();
  const [isMeta, setIsInCharacter] = useState<boolean>(false);
  const inputElement = useRef<HTMLInputElement>(null);
  const { membersTyping } = usePresence({
    inputElement,
    socket,
  });
  const { assistantIsTyping, messageList } = useQuestViewSocket();

  useQuestClubNotifications({ socket });
  useScrollToBottomOnChange(messageList);

  // Announce to the server that the user is here and ready to interact.  This
  // allows the client to reconnect to the socket if connection is momentarily
  // lost.
  useActivityAnnouncer({
    socket,
    user,
    questId,
  });

  const { isConnected } = useConnectionStatus({ socket });

  const [message, setMessage] = useState("");
  useEffect(() => {
    setMessage("");
  }, [loaderData]);

  useEffect(() => {
    if (message.match(/^\/m/)) {
      setIsInCharacter(true);
      setMessage(message.replace(/^\/m/, ""));
    }
  }, [message]);

  function onInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    setMessage(e.target.value);
  }

  function onInputKeyUp(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === "Backspace" && message === "") {
      setIsInCharacter(false);
    }
  }

  const formDisabled = assistantIsTyping || !isConnected;

  const { participants } = useParticipants({
    socket,
    initialParticipants: loaderData.quest.members,
    questOwnerId: loaderData?.quest?.ownerId || undefined,
  });

  const groupedMessages = messageList.reduce(
    (acc, message): (typeof messageList)[] => {
      if (acc.length === 0) return [[message]];
      const lastMessage = acc[acc.length - 1][0];
      if (!lastMessage) return acc;
      if (lastMessage === message) return acc;

      if (
        lastMessage.user?.id === message.user?.id &&
        lastMessage.category === MessageCategory.Meta.Player &&
        message.category === MessageCategory.Meta.Player
      ) {
        // If last message was from same user, append to last message
        acc[acc.length - 1].push(message);
      } else {
        // Otherwise, start a new group
        acc.push([message]);
      }
      return acc;
    },
    [] as (typeof messageList)[]
  );

  const messages = groupedMessages.map((messageGroup) => (
    <div
      key={messageGroup[0].id}
      className={`flex flex-col rounded p-4 ${
        messageGroup[0].user?.email ? "items-end" : "bg-slate-200 dark:bg-black"
      }`}
    >
      <h1 className="group mb-2 flex text-xl font-bold text-teal-500">
        {messageGroup[0].user?.name && loaderData.quest.ownerId === user.id && (
          <RetconForm
            messageId={messageGroup[0].id}
            className="invisible group-hover:visible"
          />
        )}
        {messageGroup[0].category === MessageCategory.Meta.Player && (
          <span className="text-slate-400">(Meta):&nbsp;</span>
        )}
        {messageGroup[0].user?.name || (
          <span>
            🤖&nbsp;<span className="underline">GM</span>
          </span>
        )}
      </h1>
      {messageGroup.map((message) => (
        <p style={{ whiteSpace: "pre-line" }} key={message.id}>
          {message.content}
        </p>
      ))}
    </div>
  ));

  return (
    <CenteredColumn>
      <QuestViewHeader
        questId={loaderData.quest.id}
        questTitle={loaderData.quest.title}
        participants={participants}
        canInvite={loaderData.quest.ownerId === user?.id}
        canEdit={loaderData.quest.ownerId === user?.id}
        summary={loaderData.summary?.content || "The journey begins..."}
      >
        <ErrorToast />
      </QuestViewHeader>
      <div className="flex flex-col ">
        {messages}
        {!isConnected && <ConnectivityIndicator />}
      </div>
      <div className="mt-4">
        {assistantIsTyping && <p>GM is responding...</p>}
        <Form method="post" className="relative mt-4 flex">
          {isMeta && (
            <label
              htmlFor="message"
              className="absolute left-1 top-1 block rounded bg-slate-500 p-1 text-slate-50 dark:bg-slate-800"
            >
              Meta:
            </label>
          )}
          <input
            type="hidden"
            name="isMeta"
            value={isMeta ? "true" : "false"}
          />
          <input type="hidden" name="socketId" value={socket?.id} />
          <input type="hidden" name="command" value="create-message" />
          <Input
            type="text"
            name="message"
            onChange={onInputChange}
            onKeyDown={onInputKeyUp}
            disabled={formDisabled}
            value={message}
            className={`mr-4 w-full ${isMeta ? "pl-14" : ""}`}
            ref={inputElement}
            autoComplete="off"
            autoFocus
          />
          <Button type="submit" className="" disabled={formDisabled}>
            Send
          </Button>
        </Form>
        <div className="relative">
          <div className="absolute">
            {!!membersTyping.length && (
              <p className="text-slate-400">
                {membersTyping.map((member) => member.name).join(", ")}{" "}
                {membersTyping.length > 1 ? "are" : "is"} typing...
              </p>
            )}
          </div>
        </div>
        <p className="mb-8 mt-6 text-slate-400">
          Hint: Use <code>/m</code> to metagame without involving the
          GM.
        </p>
      </div>
    </CenteredColumn>
  );
}
