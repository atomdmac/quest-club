import type { ActionArgs, LoaderArgs, V2_MetaFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useActionData, useLoaderData } from "@remix-run/react";
import Account from "~/components/Account";
import Heading from "~/components/Heading";
import {
  getUserByEmail,
  getUserById,
  createUserPassword,
  updateUserById,
} from "~/models/user.server";
import { getUserId, createUserSession } from "~/session.server";
import { safeRedirect, validateEmail } from "~/utils";

export const meta: V2_MetaFunction = () => [{ title: "Quests" }];

export const loader = async ({ request }: LoaderArgs) => {
  const userId = await getUserId(request);
  const user = userId ? await getUserById(userId) : null;
  if (!user) return redirect("/");
  return json({
    playerName: user.name,
    email: user.email,
  });
};

export const action = async ({ request }: ActionArgs) => {
  const userId = await getUserId(request);
  if (!userId) return new Response(null, { status: 401 });

  const formData = await request.formData();
  const playerName = formData.get("playerName");
  const email = formData.get("email");
  const password = formData.get("password") || "";
  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");

  if (typeof playerName !== "string" || playerName.length === 0) {
    return json(
      {
        errors: { playerName: "Name is required", email: null, password: null },
      },
      { status: 400 }
    );
  }

  if (!validateEmail(email)) {
    return json(
      { errors: { email: "Email is invalid", password: null } },
      { status: 400 }
    );
  }

  await updateUserById(userId, playerName, email);

  // Only update password if a new one was provided.
  if (typeof password === "string" && password.length) {
    if (password.length < 8) {
      return json(
        { errors: { email: null, password: "Password is too short" } },
        { status: 400 }
      );
    }
    await createUserPassword(userId, password);
  }

  return createUserSession({
    redirectTo,
    remember: false,
    request,
    userId,
  });
};

export default function AccountPage() {
  const userData = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();
  return (
    <div className="flex min-h-full flex-col justify-center">
      <div className="mx-auto w-full lg:max-w-screen-lg px-4">
      <Heading>Account</Heading>
      <Account
        playerName={userData.playerName}
        email={userData.email}
        errors={actionData?.errors}
      />
      </div>
    </div>
  );
}
