export type LogLevels = 0 | 1 | 2 | 3;

const LOG_LEVELS = ["fatal", "error", "warn", "info"];

class Logger {
  level: LogLevels = 1;

  setLevel(level: LogLevels) {
    this.level = level;
  }

  log(level: LogLevels, ...rest: any[]) {
    if (level <= this.level) {
      return;
    }
    console.log(`${LOG_LEVELS[level]}: `, ...rest);
  }

  info(...rest: any[]) {
    this.log(3, ...rest);
  }

  warn(...rest: any[]) {
    this.log(2, ...rest);
  }

  error(...rest: any[]) {
    this.log(1, ...rest);
  }

  fatal(...rest: any[]) {
    this.log(0, ...rest);
  }
}

let logger: Logger | undefined;

export default function getLogger() {
  if (logger) {
    return logger;
  } else {
    logger = new Logger();
    const logLevel = (parseInt(process.env.LOG_LEVEL || "", 10) ||
      0) as LogLevels;
    logger.setLevel(logLevel);
    return logger;
  }
}
