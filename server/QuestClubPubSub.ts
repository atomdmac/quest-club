import type { Server as HTTPServer } from "http";
import { Server } from "socket.io";
import type { Socket } from "socket.io";
import {
  HEALTH_CHECK,
  PRESENCE_STATUS,
  QUEST_MEMBERSHIP_STATUS,
  QUEST_MEMBERSHIP_STATUS_REQUEST,
  SOCKET_EVENTS_SERVER,
  TYPING_STATUS,
} from "~/constants";
import type { User } from "~/models/scenarios.server";

type Topic = Set<Socket>;
type Topics = Map<string, Topic>;
type SocketInfo = Map<Socket, { user: User; topic: Topic | null }>;

export class QuestClubPubSub {
  private topics: Topics;
  private socketInfo: SocketInfo;

  constructor() {
    this.topics = new Map();
    this.socketInfo = new Map();
  }

  private getOrCreateTopic(topicId: string) {
    let topic = this.topics.get(topicId) || new Set();
    this.topics.set(topicId, topic);
    return topic;
  }

  registerSocketInfo(socket: Socket, user: User) {
    console.log(
      "regsterSocketInfo::user = ",
      user.id,
      ", socket = ",
      socket.id
    );
    if (!this.socketInfo.has(socket)) {
      this.socketInfo.set(socket, {
        user,
        topic: null,
      });
    }
  }

  unregisterSocketInfo(socket: Socket) {
    console.log("unregisterSocketInfo::socket", socket.id);
    this.socketInfo.delete(socket);
  }

  findSocketByUserId(userId: string) {
    return Array.from(this.socketInfo.entries()).find(
      ([_, { user }]) => user.id === userId
    )?.[0];
  }

  joinTopic(socket: Socket, topicId: string) {
    console.log("joinTopic::", topicId);
    // Add the socket to the requested topic.
    const topic = this.getOrCreateTopic(topicId);
    topic.add(socket);

    // Record the fact that the socket has been added to the topic.
    const socketEntry = this.socketInfo.get(socket);
    if (socketEntry) {
      socketEntry.topic = topic;
    }
  }

  leaveTopic(socket: Socket, topicId: string) {
    console.log("leaveTopic::socketId", socket.id, ", topicId", topicId);
    const topic = this.topics.get(topicId);
    if (topic && topic.has(socket)) {
      topic.delete(socket);
    }
  }

  leaveAllTopics(socket: Socket) {
    this.topics.forEach((topic) => {
      if (topic.has(socket)) topic.delete(socket);
    });
  }

  broadcastToCurrentTopic(event: string, sourceSocket: Socket, data?: any) {
    console.log("broadcastToCurrentTopic::sourceSocket", sourceSocket.id);
    console.log("broadcastToCurrentTopic::event", event);
    const socketEntry = this.socketInfo.get(sourceSocket);
    console.log(
      "broadcastToCurrentTopic::socketEntry exists = ",
      !!socketEntry
    );
    if (!socketEntry?.topic) return;
    socketEntry.topic.forEach((socket: Socket) => {
      if (socket === sourceSocket) return;
      socket.emit(event, { ...data, user: socketEntry.user });
    });
  }

  broadcastToTopic(
    event: string,
    data: any,
    topicId: string,
    sourceSocketId?: string
  ) {
    console.log("broadcastToTopic::event", event);
    this.getOrCreateTopic(topicId).forEach((socket) => {
      if (socket.id === sourceSocketId) return;
      socket.emit(event, data);
    });
  }
}

export default function initializePubSub(httpServer: HTTPServer) {
  const sockets = new QuestClubPubSub();
  const io = new Server(httpServer);

  io.on("connection", (socket: Socket) => {
    console.log(socket.id, "connected");

    socket.on(SOCKET_EVENTS_SERVER.DISCONNECT, () => {
      console.log(socket.id, "disconnected");
      sockets.leaveAllTopics(socket);
      sockets.unregisterSocketInfo(socket);
    });

    socket.on(HEALTH_CHECK.PING, () => {
      console.log("ping");
      socket.emit(HEALTH_CHECK.PONG);
    });

    socket.on("identify", function (user) {
      console.log("identify::user", user.id);
      sockets.registerSocketInfo(socket, user);
    });

    socket.on(TYPING_STATUS.ACTIVE, function () {
      sockets.broadcastToCurrentTopic(TYPING_STATUS.ACTIVE, socket);
    });

    socket.on(TYPING_STATUS.IDLE, function () {
      sockets.broadcastToCurrentTopic(TYPING_STATUS.IDLE, socket);
    });

    socket.on(QUEST_MEMBERSHIP_STATUS_REQUEST.JOIN, (topicId) => {
      sockets.joinTopic(socket, topicId);
      sockets.broadcastToCurrentTopic(QUEST_MEMBERSHIP_STATUS.ACCEPTED, socket);
    });

    socket.on(QUEST_MEMBERSHIP_STATUS_REQUEST.LEAVE, (topicId) => {
      sockets.leaveAllTopics(socket);
      sockets.broadcastToCurrentTopic(QUEST_MEMBERSHIP_STATUS.LEFT, socket, {
        topicId,
      });
    });

    socket.on(PRESENCE_STATUS.ACTIVE, ({ topicId, user }) => {
      sockets.registerSocketInfo(socket, user);
      sockets.joinTopic(socket, topicId);
    });
  });

  return sockets;
}
